using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading.Tasks;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

namespace Base
{
    public class SceneTransitor : MonoBehaviour
    {
        [Header("Settings Transition")]
         [SerializeField] private float _timeFading;
         [SerializeField] private float _timeMinTransition;
         [SerializeField] private bool _havePanelLoading;
         [SerializeField] private GameObject _panelLoading;
        
        [Header("Settings ReloadScene")]
         [SerializeField] private float _timeFadingReload;

        private void OnEnable()
        {
            if (_havePanelLoading)
            {
                var LoadingCG = _panelLoading.GetComponent<CanvasGroup>();
                LoadingCG.alpha = 0f;
                _panelLoading.SetActive(false);
            }

        }
        public async void TransiteTo(string sceneName)
        {
            DontDestroyOnLoad(this.gameObject);
            var cg = GetComponent<CanvasGroup>();

            await FadeIn(cg);

            if (_havePanelLoading)
            {
                var LoadingCG = _panelLoading.GetComponent<CanvasGroup>();
                _panelLoading.SetActive(true);
                await FadeIn(LoadingCG,3);
            }

            var load = SceneManager.LoadSceneAsync(sceneName);
            var cont = 0f;
            while (!load.isDone)
            {
                cont += Time.deltaTime;
                await Task.Yield();
            }

            var timeToWait = cont < _timeMinTransition ? _timeMinTransition - cont : 0f;
            await Task.Delay((int)(timeToWait * 1000));

            if (_havePanelLoading)
            {
                var LoadingCG = _panelLoading.GetComponent<CanvasGroup>();
                await FadeOut(LoadingCG, 3);
                _panelLoading.SetActive(false);
            }


            await FadeOut(cg);
            Destroy(gameObject);
        }

        private async Task FadeIn(CanvasGroup cg, float timeMultiplier = 1f)
        {
            cg.blocksRaycasts = true;

            var currentTimefading = _timeFading * timeMultiplier;

            LeanTween.alphaCanvas(cg, 1, currentTimefading);
            await Task.Delay((int)(currentTimefading * 1000));
        }
        private async Task FadeOut(CanvasGroup cg, float timeMultiplier = 1f)
        {

            var currentTimefading = _timeFading * timeMultiplier;

            LeanTween.alphaCanvas(cg, 0, currentTimefading);
            await Task.Delay((int)(currentTimefading * 1000));

            cg.blocksRaycasts = false;
        }

        public async void ExitGame()
        {
            await FadeIn(GetComponent<CanvasGroup>(), 2f);

            #if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
            #else
                Application.Quit();
            #endif
        }

        public async void ReloadScene()
        {
            DontDestroyOnLoad(this.gameObject);
            await FadeIn(GetComponent<CanvasGroup>(), _timeFadingReload/2);
            var load = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);
            while (!load.isDone)
            {
                await Task.Yield();
            }
            await FadeOut(GetComponent<CanvasGroup>(), _timeFadingReload / 2);
            Destroy(gameObject);
        }

    }

}
