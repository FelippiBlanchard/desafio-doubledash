using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Base.VisualTime
{
    public class ChangeAlphaByTime : MonoBehaviour
    {
        [Header("Day Sprites")]
         [SerializeField] private List<Image> _images;

        private void Update()
        {
            ApplyCurrentTime();
        }
        public void ApplyCurrentTime()
        {
            var alpha = TimeManager.Instance.AlphaDay;
            foreach (var image in _images)
            {
                var temp = image.color;
                temp.a = alpha;
                image.color = temp;
            }
        }

    }
}
