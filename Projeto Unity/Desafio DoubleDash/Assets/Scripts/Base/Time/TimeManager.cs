using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace Base.VisualTime
{
    public class TimeManager : MonoBehaviour
    {
        [Header("Settings")]
         [SerializeField, Range(0.1f, 3f)] public float Multiplier;
         [SerializeField, Range(0f, 24f)] private float _startTime;
        
        [Header("Timings")]
         [SerializeField] private float _dayBegin;
         [SerializeField] private float _beginningDuration;
         [SerializeField] private float _dayEnd;
         [SerializeField] private float _endingDuration;

        public float Time;
        public float AlphaDay;
        public static TimeManager Instance;
        
        private bool _pause;

        private void Awake()
        {
            if(Instance == null)
            {
                Time = _startTime;
                DontDestroyOnLoad(gameObject);
                Instance = this;
            }
            Instance.Multiplier = Multiplier; //Each Scenes with this script can have a multiplier time, so, menu can be faster than game
        }
        private void Update()
        {
            if (!_pause)
            {
                Time = Time > 24 ? 0 : Time + UnityEngine.Time.deltaTime * Multiplier;
                AlphaDay = ConvertTimeToAlpha(Time);
            }
        }

        public void PauseTime(bool pause)
        {
            Instance._pause = pause;
        }

        private float ConvertTimeToAlpha(float time)
        {
            if (time < _dayBegin && time > _dayBegin - _beginningDuration)
            {
                return ((time - (_dayBegin - _beginningDuration)) / (_beginningDuration));
            }
            if (time > _dayEnd && time < _dayEnd + _endingDuration)
            {
                return (1 - (time - _dayEnd) / (_endingDuration));
            }


            if (time > _dayBegin && time < _dayEnd)
            {
                return 1;
            }
            return 0;

        }

    }

}
