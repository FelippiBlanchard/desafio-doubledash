using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Events;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Base.Audio
{
    public class AudioManager : MonoBehaviour
    {
        [SerializeField] private List<SingleAudio> _audios;
        [SerializeField] private AudioSource _backgroundMusic;
        [SerializeField] private AudioMixer _mixer;
        [SerializeField] private Slider _sliderVolume;

        private static AudioManager Instance;

        private void Start()
        {
            InitializeVolume();
            InitializeBackgroundMusic();
        }
        private void InitializeBackgroundMusic()
        {
            if (Instance == null)
            {
                if(_backgroundMusic != null)
                {
                    DontDestroyOnLoad(gameObject);
                    _backgroundMusic.Play();
                    _backgroundMusic.loop = true;
                    Instance = this;
                }
            }
            else
            {
                Instance._backgroundMusic.UnPause();
                if (_backgroundMusic != null && _backgroundMusic.isPlaying) _backgroundMusic.Stop();
            }
        }
        public void Play(string name)
        {
            _audios.Find(audio => audio.name == name).source.Play();
        }

        public void PauseAll()
        {
            foreach (var audio in _audios)
            {
                audio.source.Pause();
            }
            Instance._backgroundMusic.Pause();
        }

        public void ResumeAll()
        {
            foreach (var audio in _audios)
            {
                audio.source.UnPause();
            }
            Instance._backgroundMusic.UnPause();
        }

        public void SetVolume(float value)
        {
            var volume = 0f;
            if (value < 0.5f)
            {
                volume = value * 160 - 80;
            }
            if (value >= 0.5f)
            {
                volume = value * 40 - 20;
            }

            _mixer.SetFloat("Volume", volume);
            PlayerPrefs.SetFloat("Volume", volume);
        }

        public void InitializeVolume()
        {
            var volume = PlayerPrefs.GetFloat("Volume", 0f);
            var value = volume < 0.5f ? ((volume + 80) / 160) : ((volume + 20) / 40);
            SetVolume(value);
            if(_sliderVolume != null)
            {
                _sliderVolume.onValueChanged.AddListener(SetVolume);
                _sliderVolume.value = value;
            }
        }

    }

    [System.Serializable]
    public class SingleAudio
    {
        public string name;
        public AudioSource source;
    }

}
