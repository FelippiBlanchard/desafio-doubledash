using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using UnityEngine.Serialization;

namespace Base.Animations
{
    [RequireComponent(typeof(CanvasGroup))]
    public class FadeAnimation : MonoBehaviour
    {
        [Header("Fade Settings")]
         [SerializeField] private bool _playOnEnable;
         [SerializeField, Tooltip("Wait this seconds before do the animation")] private float _waitBeforeFade;
         [SerializeField] private float _fadeDuration;
        
        [Header("Flash Settings")]
         [SerializeField] private float _flashMaxAlpha;
         [SerializeField] private float _flashDuration;

        private async void Awake()
        {
            if (_playOnEnable)
            {
                await Fade(0f, 0f);
            }
        }
        private async void OnEnable()
        {
            if (_playOnEnable)
            {
                await Task.Delay((int)(_waitBeforeFade * 1000));
                await Fade(1, _fadeDuration);
            }
        }
        public async void FadeOutAndDisable()
        {
            await Fade(0, _fadeDuration);
            gameObject.SetActive(false);
        }
        public async Task Fade(float alpha, float duration)
        {
            var cg = GetComponent<CanvasGroup>();
            cg.LeanAlpha(alpha, duration);
            await Task.Delay((int)(duration * 1000));
            cg.interactable = alpha > 0;
            cg.blocksRaycasts = alpha > 0;
        }

        public async void FadeFlash()
        {
            await Fade(_flashMaxAlpha, _flashDuration / 2);
            await Fade(0f, _flashDuration / 2);
        }

    }

}
