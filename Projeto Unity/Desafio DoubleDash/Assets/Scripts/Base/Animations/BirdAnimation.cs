using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Base.Data;
using UnityEngine.Serialization;

namespace Base.Animations
{
    public class BirdAnimation : MonoBehaviour
    {

        [Header("Settings")]
         [SerializeField] private float _speed;
         [SerializeField] private float _height;

        private CharacterSprites _characterSprites;
        
        private void Start()
        {
            UpdateCurrentCharacterSprite();
            StartCoroutine(AnimationBird());
        }

        public void UpdateCurrentCharacterSprite()
        {
            _characterSprites = DataSpritesController.GetCurrentCharacterSprites();
        }

        private IEnumerator AnimationBird()
        {
            var image = GetComponent<Image>();
            var initialPosition = transform.localPosition.y;

            while (gameObject.activeSelf)
            {
                image.sprite = _characterSprites.Downflap;
                LeanTween.moveLocalY(gameObject, initialPosition + _height, 1/_speed);
                yield return new WaitForSeconds(1/_speed);

                image.sprite = _characterSprites.Midflap;
                yield return new WaitForSeconds(0.3f / _speed);
                
                image.sprite = _characterSprites.Upflap;
                LeanTween.moveLocalY(gameObject, initialPosition, 1 / _speed);
                yield return new WaitForSeconds(1 / _speed);
            }


        }

    }

}
