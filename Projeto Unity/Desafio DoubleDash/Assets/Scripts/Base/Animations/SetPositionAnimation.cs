using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using UnityEngine.Serialization;

namespace Base.Animations
{
    public class SetPositionAnimation : MonoBehaviour
    {
        [Header("Set Position Settings")]
         [SerializeField] private bool _playOnEnable;
         [SerializeField] private bool _setY;
         [SerializeField] private bool _setX;
         [SerializeField, Tooltip("Wait this seconds before do the animation")] private float _waitBeforeSet;
         [SerializeField] private float _setPositionDuration;
         [Tooltip("If Play On Enable is true, when start the game the object will have this position")]
          [SerializeField] private Vector2 _startPosition;
        private Vector2 _initialPosition;

        private void Awake()
        {
            _initialPosition = transform.localPosition;
            if (_playOnEnable)
            {
                if(_setX && _setY) { transform.localPosition = _startPosition; }
                else { if (_setX) { transform.localPosition = new Vector2(_startPosition.x, transform.localPosition.y); }
                    else { if (_setY) { transform.localPosition = new Vector2(transform.localPosition.x, _startPosition.y); } } }
            }
        }
        private async void OnEnable()
        {
            if (_playOnEnable)
            {
                await Task.Delay((int)(_waitBeforeSet * 1000));
                SetInitialPosition();
            }
        }

        public async void SetStartPositionAndDisable()
        {

            if(_setX && _setY) { transform.LeanMoveLocal(_startPosition, _setPositionDuration).setEaseInOutSine();}
                else { if (_setX) { transform.LeanMoveLocalX(_startPosition.x, _setPositionDuration).setEaseInOutSine(); }
                    else { if (_setY) { transform.LeanMoveLocalY(_startPosition.y, _setPositionDuration).setEaseInOutSine(); } } }

            await Task.Delay((int)(_setPositionDuration * 1000));
            gameObject.SetActive(false);
        }

        public async void SetInitialPosition()
        {
            transform.LeanMoveLocal(_initialPosition, _setPositionDuration).setEaseInOutSine();
            await Task.Delay((int)(_setPositionDuration * 1000));
        }

    }
}
