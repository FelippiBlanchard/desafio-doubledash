using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace Base.Animations
{
    public class DayTransitor : MonoBehaviour
    {
        [SerializeField] private float _timeTransition;
        [SerializeField] private float _timeBeforeTransition;

        private void Start()
        {
            StartCoroutine(TransitionCoroutine());
        }
        private IEnumerator TransitionCoroutine()
        {
            DontDestroyOnLoad(gameObject);

            var cgDay = GetComponentInChildren<CanvasGroup>();

            bool transiteDay = false;
            while (Application.isPlaying)
            {
                yield return new WaitForSeconds(_timeBeforeTransition);
                if (transiteDay)
                {
                    LeanTween.alphaCanvas(cgDay, 1, _timeTransition);
                }
                else
                {
                    LeanTween.alphaCanvas(cgDay, 0, _timeTransition);
                }
                yield return new WaitForSeconds(_timeTransition);
                transiteDay = !transiteDay;
                yield return null;
            }


        }


    }

}
