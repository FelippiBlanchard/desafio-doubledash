using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace Base
{
    public class ScoresManager : MonoBehaviour
    {
        [Space(10)]
        [SerializeField] private bool _setBestScoreOnEnable;
        [SerializeField] private bool _setCurrentScoreOnEnable;
        
        [Header("TextBox")]
         [SerializeField] private List<TextMeshProUGUI> _currentScoreText;
         [SerializeField] private List<TextMeshProUGUI> _bestScoreText;
        
        [Header("Events")]
         [SerializeField, Tooltip("if best score is zero, the event will set true")] private UnityEvent<bool> _onSetBestScoreOnEnable;
         [SerializeField] private UnityEvent _whenSetNewBestScore;
        
         private int _currentScore;

        private void OnEnable()
        {
            if (_setBestScoreOnEnable)
            {
                if (PlayerPrefs.GetInt("BestScore", 0) == 0)
                {
                    _onSetBestScoreOnEnable.Invoke(false);
                }
                else
                {
                    GetBestScore();
                    _onSetBestScoreOnEnable.Invoke(true);
                }
            }
            if (_setCurrentScoreOnEnable) SetCurrentScoreAndAdd(0);
        }
        public void GetBestScore()
        {
            foreach (var bestScore in _bestScoreText)
            {
                bestScore.text = PlayerPrefs.GetInt("BestScore", 0).ToString(); ;
            }

        }
        
        public void TrySetBestScore()
        {
            if(_currentScore > PlayerPrefs.GetInt("BestScore", 0))
            {

                PlayerPrefs.SetInt("BestScore", _currentScore);
                _whenSetNewBestScore.Invoke();
            }
        }

        public void SetCurrentScoreAndAdd(int value)
        {
            _currentScore += value;
            foreach(var score in _currentScoreText)
            {
                score.text = _currentScore.ToString();
            }
        }
    }
}
