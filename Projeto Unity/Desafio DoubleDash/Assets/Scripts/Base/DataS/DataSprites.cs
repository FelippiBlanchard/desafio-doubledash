using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace Base.Data
    {

    [CreateAssetMenu(fileName = "DataSprites", menuName = "ScriptableObjects/DataSprites", order = 1)]
    public class DataSprites : ScriptableObject
    {
        public List<Sprite> Downflap;
        public List<Sprite> Midflap;
        public List<Sprite> Upflap;
    }
}
