using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

namespace Base.Data
{
    public class DataSpritesController : MonoBehaviour
    {
        [SerializeField] private DataSprites _dataSprites;

        public static DataSprites StaticData;
        public static int CurrentIndex;

        private void Awake()
        {
            StaticData = _dataSprites;
            CurrentIndex = PlayerPrefs.GetInt("IndexSprites", 0);
        }
        public void ChangeSprite()
        {
            CurrentIndex = PlayerPrefs.GetInt("IndexSprites", 0);

            CurrentIndex = CurrentIndex + 1 >= StaticData.Downflap.Count ? 0 : CurrentIndex + 1;
            
            PlayerPrefs.SetInt("IndexSprites", CurrentIndex);

        }

        public static CharacterSprites GetCurrentCharacterSprites()
        {
            var characterSprites = new CharacterSprites(StaticData.Downflap[CurrentIndex], StaticData.Midflap[CurrentIndex],
                StaticData.Upflap[CurrentIndex]);
            return characterSprites;
        }

    }

    public class CharacterSprites
    {
        public Sprite Downflap;
        public Sprite Midflap;
        public Sprite Upflap;

        public CharacterSprites(Sprite downflap, Sprite midflap, Sprite upflap)
        {
            Downflap = downflap;
            Midflap = midflap;
            Upflap = upflap;
        }
    }

}

