using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Threading.Tasks;
using Base.Data;
using UnityEngine.Serialization;

namespace PreScene
{
    public class PreSceneStructure : MonoBehaviour
    {
        [Header("Times")]
         [SerializeField] private float _beforeSecondStart;
         [SerializeField] private float _beforeThirdStart;
        
        [Header("Events")]
         [SerializeField] private UnityEvent _onStart;
         [SerializeField] private UnityEvent _onSecondStart;
         [SerializeField] private UnityEvent _onThirdStart;

        private async void Start()
        {
            #if UNITY_ANDROID
                    Screen.SetResolution(288, 512, true);
            #endif


            _onStart.Invoke();

            await Task.Delay((int)(1000 * _beforeSecondStart));

            _onSecondStart.Invoke();

            await Task.Delay((int)(1000 * _beforeThirdStart));

            _onThirdStart.Invoke();

        }
    }
}
