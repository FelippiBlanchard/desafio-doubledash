using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using UnityEngine.Serialization;

namespace Game.Map
{
    public class PipeSpawner : MonoBehaviour
    {
        [Header("Time Settings")]
         [SerializeField] private float _waitBeforeFirstPipe;
         [SerializeField, Tooltip("Horizontal Distance between pipes")] private float _distanceBetweenPipes;
         [SerializeField] private float _velocity;
        
        [Header("Position Settings")]
         [SerializeField] private float _maxHeight;
         [SerializeField] private float _minHeight;
         [SerializeField] private float _initialXPositionSpawn;
         [SerializeField] private float _finalXPositionSpawn;
         
        [Header("Other")]
         [SerializeField] private int _qtyPipes;
         [SerializeField] private GameObject _pipePrefab;
         [SerializeField] private Transform _pipesParent;

        private List<GameObject> _spawnList = new List<GameObject>();

        private bool _paused;


        public void StartSpawn()
        {
            SpawnPipes();
            StartCoroutine(WalkPipes());
        }


        private void SpawnPipes()
        {
            for(int i = 1; i <= _qtyPipes; i++)
            {
                InstantiatePipeAt(i * _initialXPositionSpawn);
            }
        }

        private IEnumerator WalkPipes()
        {
            yield return new WaitForSeconds(_waitBeforeFirstPipe);
            while (Application.isPlaying)
            {
                if (!_paused)
                {
                    if (_spawnList.Count > 0)
                    {
                        foreach (var item in _spawnList)
                        {
                            var position = item.transform.localPosition;
                            item.transform.localPosition = new Vector3(position.x - Time.deltaTime * _velocity, position.y, position.z);
                            if (item.transform.localPosition.x < _finalXPositionSpawn)
                            {
                                var randomHeight = Random.Range(_minHeight, _maxHeight);
                                item.transform.localPosition = new Vector3((_qtyPipes-1 ) * _initialXPositionSpawn, randomHeight, position.z);
                            }
                        }
                    }
                }
                yield return null;
            }
        }

        private void InstantiatePipeAt(float xPosition)
        {
            var newPipe = Instantiate(_pipePrefab, _pipesParent);
            _spawnList.Add(newPipe);

            var randomHeight = Random.Range(_minHeight, _maxHeight);
            newPipe.transform.localPosition = new Vector3(xPosition, randomHeight, 0);
        }

        public void PausePipes()
        {
            _paused = true;
        }

        public void ResumePipes()
        {
            _paused = false;
        }
        
    }

}
