using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using UnityEngine.Serialization;

namespace Game.Map
{
    public class Ground : MonoBehaviour
    {
         [SerializeField] private float _velocity;
         [SerializeField] private float _initialPositionX;
         [SerializeField] private float _finalPositionX;

        private bool _paused;

        public async void StartWalking()
        {
            _paused = false;
            while (Application.isPlaying)
            {
                if (!_paused)
                {
                    transform.localPosition = new Vector3(transform.localPosition.x - Time.deltaTime * _velocity, transform.localPosition.y, transform.localPosition.z);

                    if(transform.localPosition.x < _finalPositionX)
                    {
                        transform.localPosition = new Vector3(_initialPositionX, transform.localPosition.y, transform.localPosition.z);
                    }
                }
                await Task.Yield();
            }
        }
        public void ResumeWalking()
        {
            _paused = false;
        }
        public void PauseWalking()
        {
            _paused = true;
        }
    }
}