using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace Game.Player
{
    [RequireComponent(typeof(Rigidbody2D))]
    
    public class PlayerPhysics : MonoBehaviour
    {
        [Header("Settings")]
         [Tooltip("Max Height the Bird reach when Jump")]
          [SerializeField] private float _maxHeight;
         [Tooltip("Duration to leave the starting height, reach the Max Height and return")]
          [SerializeField, Range(0.1f, 2f)] private float _durationJump;

        private float _maxVelocity;
        private float _fixedGravity;
        private float _currentGravity;

        private GravityState _gravityState;

        private Rigidbody2D _rgbd;

        private void Awake()
        {
            _rgbd = GetComponent<Rigidbody2D>();
            _rgbd.bodyType = RigidbodyType2D.Kinematic;

            _fixedGravity = (2 * _maxHeight) / Mathf.Pow(_durationJump/2, 2);
            _maxVelocity = _fixedGravity * _durationJump/2;
        }


        private void FixedUpdate()
        {
            Gravity();
        }


        public void StopInAir()
        {
            _rgbd.velocity = new Vector2 (0, 0);
            DisableGravity();
        }


        public void Jump()
        {
             _rgbd.velocity = new Vector2(_rgbd.velocity.x, _maxVelocity);
        }

        public void MiniJump()
        {
            _rgbd.velocity = new Vector2(_rgbd.velocity.x, _maxVelocity/2);
        }


        public void SetFalling()
        {
            _rgbd.velocity = new Vector2(0, -_maxVelocity);
        }

        private void Gravity()
        {
            if(_gravityState == GravityState.ENABLED)
            {
                _rgbd.velocity -= new Vector2(_rgbd.velocity.x, _currentGravity * Time.fixedDeltaTime);

                if (_rgbd.velocity.y < -_maxVelocity)
                {
                    _currentGravity = 0f;
                }
                else
                {
                    _currentGravity = _fixedGravity;
                }
            }
        }



        #region Machine States
        //------------Machine States-------------------------------------------------------------------------------------------------//

        public void EnableGravity()
        {
            _gravityState = GravityState.ENABLED;
        }
        public void DisableGravity()
        {
            _gravityState = GravityState.DISABLED;
        }

        private enum GravityState { ENABLED, DISABLED}




        #endregion


    }
}
