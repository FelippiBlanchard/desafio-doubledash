using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace Game.Player
{
    [RequireComponent(typeof(PlayerPhysics))]
    [RequireComponent (typeof(PlayerAnimator))]
    public class PlayerManager : MonoBehaviour
    {
        [Header("Events")]
         [SerializeField] private UnityEvent _initialize;
         [SerializeField] private UnityEvent _onBegin;
         [SerializeField] private UnityEvent _onJump;
         [SerializeField] private UnityEvent _onDie;
         [SerializeField] private UnityEvent _onPause;
         [SerializeField] private UnityEvent _onResume;
         [SerializeField] private UnityEvent _onReachObjective;

        private JumpState _jumpState;

        public void CallInitializeEvent() { _initialize.Invoke(); }
        public void CallBeginEvent() { _onBegin.Invoke(); }
        public void CallJumpEvent() { if(_jumpState == JumpState.ENABLED) _onJump.Invoke(); }
        public void CallDieEvent() { _onDie.Invoke(); }
        public void CallResumeEvent() { _onResume.Invoke(); }
        public void CallPauseEvent() { _onPause.Invoke(); }
        public void CallReachObjectiveEvent() { _onReachObjective.Invoke(); }

        public void EnableJump() { _jumpState = JumpState.ENABLED; }
        public void DisableJump() { _jumpState = JumpState.DISABLED; }
        private enum JumpState { ENABLED, DISABLED}


    }

}
