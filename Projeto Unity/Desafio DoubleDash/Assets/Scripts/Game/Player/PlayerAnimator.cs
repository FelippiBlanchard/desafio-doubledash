using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading.Tasks;
using Base.Data;
using UnityEngine.Serialization;

namespace Game.Player
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class PlayerAnimator : MonoBehaviour
    {
        [SerializeField] private Image _image;
        
        [Header("Die Settings")]
         [SerializeField] private float _angleDie;
         [SerializeField, Range(0.1f, 1.5f)] private float _durationDie;
         
        [Header("Simple fly Settings")]
         [SerializeField, Range(5, 50)] private float _speedSimpleFly;
        
        [Header("Jump Settings")]
         [SerializeField, Range(0.1f, 2f)] private float _durationJump;
         [SerializeField] private float _angleJump;


        private Coroutine _simpleFlyCoroutine;
        private Coroutine _jumpCoroutine;
        
        private CharacterSprites _characterSprites;


        private void Start()
        {
            GetCurrentCharacterSprite();
        }

        private void GetCurrentCharacterSprite()
        {
            _characterSprites = DataSpritesController.GetCurrentCharacterSprites();
        }

        public void JumpAnimation()
        {
            TryStopCoroutine(_jumpCoroutine);
            _jumpCoroutine = StartCoroutine(Jump());
        }
        private IEnumerator Jump()
        {
            LeanTween.cancel(_image.transform.gameObject);
            _image.transform.eulerAngles = new Vector3(0,0,_angleJump);
            yield return new WaitForSeconds(_durationJump/2);
            _image.transform.LeanRotateZ(-_angleJump, _durationJump / 2);
            yield return new WaitForSeconds(_durationJump / 2);
        }

        public void StartSimpleFlyAnimation()
        {
            _simpleFlyCoroutine = StartCoroutine(SimpleFly());
        }
        private IEnumerator SimpleFly()
        {
            if (_characterSprites == null) { GetCurrentCharacterSprite(); }
            while (Application.isPlaying)
            {
                _image.sprite = _characterSprites.Downflap;
                yield return new WaitForSeconds(1 / _speedSimpleFly);
                _image.sprite = _characterSprites.Midflap;
                yield return new WaitForSeconds(1 / _speedSimpleFly);
                _image.sprite = _characterSprites.Upflap;
                yield return new WaitForSeconds(1 / _speedSimpleFly);
                _image.sprite = _characterSprites.Midflap;
                yield return new WaitForSeconds(1 / _speedSimpleFly);

            }
        }

        public void FallingAnimation()
        {
            _image.transform.LeanRotateZ(-_angleJump, _durationJump / 2);
        }
        public void DieAnimation()
        {
            TryStopCoroutine(_simpleFlyCoroutine);
            TryStopCoroutine(_jumpCoroutine);
            LeanTween.cancel(_image.transform.gameObject);
            _image.transform.LeanRotateZ(_angleDie, _durationDie);
        }

        public void StopInAirAnimation()
        {
            TryStopCoroutine(_simpleFlyCoroutine);
            TryStopCoroutine(_jumpCoroutine);
            LeanTween.cancel(_image.transform.gameObject);
        }

        private void TryStopCoroutine(Coroutine coroutine)
        {
            if(coroutine != null) StopCoroutine(coroutine);
        }

    }

}

