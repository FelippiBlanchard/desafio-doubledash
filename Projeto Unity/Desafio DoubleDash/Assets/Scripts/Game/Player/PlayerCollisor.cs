using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace Game.Player
{
    [RequireComponent(typeof(Collider2D))]
    public class PlayerCollisor : MonoBehaviour
    {
        [Header("Tags")]
         [SerializeField] private string _deadlyTag;
         [SerializeField] private string _objectiveTag;
         [SerializeField] private string _groundTag;
         
        [Header("OnTrigger Events")]
         [SerializeField] private UnityEvent _onCollisionWithDeadlyTag;
         [SerializeField] private UnityEvent _onCollisionWithObjectiveTag;
         [SerializeField] private UnityEvent _onCollisionWithGroundTag;
         
        [Header("OnExit Events")]
         [SerializeField] private UnityEvent _onExitWithDeadlyTag;
         [SerializeField] private UnityEvent _onExitWithObjectiveTag;
         [SerializeField] private UnityEvent _onExitWithGroundTag;

        private CollisorState _state;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if(_state == CollisorState.ENABLED)
            {
                var colTag = collision.gameObject.tag;
                switch (colTag)
                {
                    case string tag when tag.Contains(_deadlyTag):
                        _onCollisionWithDeadlyTag.Invoke();
                        break;
                    case string tag when tag.Contains(_objectiveTag):
                        _onCollisionWithObjectiveTag.Invoke();
                        break;
                    case string tag when tag.Contains(_groundTag):
                        _onCollisionWithGroundTag.Invoke();
                        break;
                    default:
                        break;
                }
            }
            else
            {
                var colTag = collision.gameObject.tag;
                switch (colTag)
                {
                    case string tag when tag.Contains(_groundTag):
                        _onCollisionWithGroundTag.Invoke();
                        break;
                    default:
                        break;
                }
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (_state == CollisorState.ENABLED)
            {
                var colTag = collision.gameObject.tag;
                switch (colTag)
                {
                    case string tag when tag.Contains(_deadlyTag):
                        _onExitWithDeadlyTag.Invoke();
                        break;
                    case string tag when tag.Contains(_objectiveTag):
                        _onExitWithObjectiveTag.Invoke();
                        break;
                    case string tag when tag.Contains(_groundTag):
                        _onExitWithGroundTag.Invoke();
                        break;
                    default:
                        break;
                }
            }
            else
            {
                var colTag = collision.gameObject.tag;
                switch (colTag)
                {
                    case string tag when tag.Contains(_groundTag):
                        _onExitWithGroundTag.Invoke();
                        break;
                    default:
                        break;
                }
            }
        }

        public void EnableCollisor()
        {
            _state = CollisorState.ENABLED;
        }
        public void DisableCollisor()
        {
            _state = CollisorState.DISABLED;
        }
        private enum CollisorState { ENABLED, DISABLED}

    }
}
