using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace Game
{
    public class GameStructure : MonoBehaviour
    {
        [SerializeField] private UnityEvent _onStart;
        [SerializeField] private UnityEvent _onBegin;
        [SerializeField] private UnityEvent _onAchieveObjective;
        [SerializeField] private UnityEvent _onPause;
        [SerializeField] private UnityEvent _onResume;
        [SerializeField] private UnityEvent _onDie;
        [SerializeField] private UnityEvent _onGameOver;


        private bool _paused;
        private bool _gameOver;

        private void Start() { _onStart.Invoke(); }
        public void CallBeginEvent() { _onBegin.Invoke(); }
        public void CallDieEvent() { _onDie.Invoke(); }
        public void CallGameOverEvent() { _onGameOver.Invoke(); _gameOver = true; }

        public void CallResumeEvent() { if (!_gameOver) _onResume.Invoke(); _paused = false; }

        public void CallPauseEvent() { if(!_gameOver) _onPause.Invoke(); _paused = true; }

        public void CallAchieveObjectiveEvent() { _onAchieveObjective.Invoke(); }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (!_paused)
                {
                    CallPauseEvent();
                }
                else
                {
                    CallResumeEvent();
                }
            }
        }
    }
}
