using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace Game
{
    public class TapArea : MonoBehaviour, IPointerDownHandler
    {
        [SerializeField] private UnityEvent _onFirstTap;
        [SerializeField] private UnityEvent _onTap;

        private bool _firstTap = true;
        
        public void OnPointerDown(PointerEventData eventData)
        {
            Tap();
        }
        
        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
            {
                Tap();
            }
        }

        private void Tap()
        {
            if (_firstTap)
            {
                _onFirstTap.Invoke();
                _firstTap = false;
            }
            else
            {
                _onTap?.Invoke();
            }
        }
    }
}
