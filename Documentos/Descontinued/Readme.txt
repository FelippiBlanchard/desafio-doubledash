FirstConception é um print do diagrama inicial que eu fiz para facilitar na implementação,

por ser uma implementação feita somente por mim e com um tempo reduzido para o desenvolvimento, não me importei nele ser completo e legível por terceiros,

apesar disso, eu deixei este print para mostrar como funciona meu pensamento inicial mesmo sem a interação com terceiros.


Por último, quero ressaltar que eu não havia experiência com diagramas, então não segue nenhum padrão UML.